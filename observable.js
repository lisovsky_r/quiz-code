const defaultCloneValue = (v) => {
  return JSON.parse(JSON.stringify(v));
}

class Observable {
  constructor(value, cloneValue) {
    this._cloneValue = cloneValue || defaultCloneValue;
    this._curValue = this._cloneValue(value);
    this._listeners = [];
  }

  /**
   * Adds a listener that is notified of value changes (when `fireChanged()` is called).
   * A listener is a function that accepts the old value and the new value.
   * If the listener needs to unsubscribe, it can return `false`, all other values
   * will be ignored.
   * Parameters: listener function.
   */
  addListener(listenerFun) {
    this._listeners.push({f: listenerFun, delay: null});
  }

  /**
   * Adds an asynchronous listener that is notified of value changes (when `fireChanged()` is called)
   * not more frequently than once in `delay` ms.
   * A listener is a function that accepts the old value and the new value.
   * If the listener needs to unsubscribe, it can return `false`, all other values
   * will be ignored.
   * Parameters: listener function, delay in ms.
   */
  addAsyncListener(listenerFun, delay) {
    this._listeners.push({f: listenerFun, delay: delay});
  }

  removeListener(listenerFun) {
    this._listeners = this._listeners.filter(listener => listener.f !== listenerFun);
  }

  callListener(listener, oldValue, newValue) {
    if (listener.f(oldValue, newValue) === false) {
      this.removeListener(listener);
    }
  }

  // this could be combined to one callListener function with additional
  callListenerAsync(listener, newValue) {
    if (listener.job) {
      return
    }
    listener.job = setTimeout(() => {
      listener.job = null;
    }, listener.delay);
    this.callListener(listener, listener.oldValue, newValue);
    if (listener) {
      listener.oldValue = newValue;
    }
  }

  fireChanged(newValue) {
    let curValue = this._curValue;
    let listeners = this._listeners;
    listeners.forEach(listener => {
      if (!listener.delay) {
        this.callListener(listener, curValue, newValue);
      } else {
        if (!listener.oldValue) {
          listener.oldValue = curValue
        }
        this.callListenerAsync(listener, newValue)
      }
    });
    this._curValue = this._cloneValue(newValue);
  }
}

export default Observable;